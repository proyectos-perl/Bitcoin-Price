#!perl -T
use 5.10.0;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Finance::Bitcoin::Price::Surbitcoin' ) || print "Bail out!\n";
}

diag( "Testing Finance::Bitcoin::Price::Surbitcoin $Finance::Bitcoin::Price::Surbitcoin::VERSION, Perl $], $^X" );
