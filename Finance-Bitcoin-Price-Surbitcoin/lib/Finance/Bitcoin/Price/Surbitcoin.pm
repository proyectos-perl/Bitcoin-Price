package Finance::Bitcoin::Price::Surbitcoin;

use Moo;
use 5.10.0;
use JSON;
use Data::Dumper;
extends 'Finance::Bitcoin::Price::Api';

=head1 NAME

Finance::Bitcoin::Price::Surbitcoin - The great new Finance::Bitcoin::Price::Surbitcoin!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';
has '+url_current_price' => ( default => sub { 'https://api.blinktrade.com/api/v1/VEF/ticker' } );
has '+url_historical_price' => ( default => sub { 'https://api.blinktrade.com/api/v1/' } );
has '+source_name'          => ( default => sub { 'SurBitcoin' } );
has '+url_source'           => ( default => sub { 'http://www.surbitcoin.com' } );

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Finance::Bitcoin::Price::Surbitcoin;

    my $foo = Finance::Bitcoin::Price::Surbitcoin->new();
    ...

=head1 SUBROUTINES/METHODS

=cut

sub get_current_price {

=head2 get_current_price


=cut

	my ( $self, $currency ) = @_;

	$self->SUPER::get_current_price($currency);
	my %data;
	if ( $self->is_success ) {
		my $temp = from_json( $self->raw_data );

		if ( $temp->{sell} ) {
			$data{currency}    = uc($currency);
			$data{sell}        = $temp->{sell};
			$data{buy}         = $temp->{buy};
			$data{description} = '';
			$data{symbol}      = $data{currency};
		} else {
			$self->error_code(1);
			$self->error_message("This Currency not exist : $currency");
		}
		;    # End of if
	}
	;        # End of if

	return %data;

	# End of sub get_current_price
}

sub get_historical_price {

=head2 get_historical_price


=cut

	my ( $self, $currency ) = @_;

	$self->_set_error_code(-2);
	$self->_set_error_message('This API not provide historical data.');

	return undef;

	# End of sub get_historical_price
}

=head1 AUTHOR

Nelo R. Tovar, C<< <tovar.nelo at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-finance-bitcoin-price-surbitcoin at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Finance-Bitcoin-Price-Surbitcoin>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Finance::Bitcoin::Price::Surbitcoin


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Finance-Bitcoin-Price-Surbitcoin>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Finance-Bitcoin-Price-Surbitcoin>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Finance-Bitcoin-Price-Surbitcoin>

=item * Search CPAN

L<http://search.cpan.org/dist/Finance-Bitcoin-Price-Surbitcoin/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Nelo R. Tovar.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1;    # End of Finance::Bitcoin::Price::Surbitcoin
