#!perl -T
use 5.10.0;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Finance::Bitcoin::Price::Lakebtc' ) || print "Bail out!\n";
}

diag( "Testing Finance::Bitcoin::Price::Lakebtc $Finance::Bitcoin::Price::Lakebtc::VERSION, Perl $], $^X" );
