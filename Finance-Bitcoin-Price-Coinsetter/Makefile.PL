use 5.10.0;
use strict;
use warnings;
use ExtUtils::MakeMaker;

WriteMakefile(
    NAME             => 'Finance::Bitcoin::Price::Coinsetter',
    AUTHOR           => q{Nelo R. Tovar <tovar.nelo@gmail.com>},
    VERSION_FROM     => 'lib/Finance/Bitcoin/Price/Coinsetter.pm',
    ABSTRACT_FROM    => 'lib/Finance/Bitcoin/Price/Coinsetter.pm',
    LICENSE          => 'artistic_2',
    PL_FILES         => {},
    MIN_PERL_VERSION => 5.10.0,
    CONFIGURE_REQUIRES => {
        'ExtUtils::MakeMaker' => 0,
    },
    BUILD_REQUIRES => {
        'Test::More' => 0,
    },
    PREREQ_PM => {
        #'ABC'              => 1.6,
        #'Foo::Bar::Module' => 5.0401,
    },
    dist  => { COMPRESS => 'gzip -9f', SUFFIX => 'gz', },
    clean => { FILES => 'Finance-Bitcoin-Price-Coinsetter-*' },
);
